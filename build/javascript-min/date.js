/*! bui - v0.0.1 - 2019-04-14 
Copyright (c): kevin.huang www.vvui.net 
Released under MIT License*/

!function(e,t){"function"==typeof define&&define.amd?define(["$B"],function(n){return t(e,n)}):(e.$B||(e.$B={}),t(e,e.$B))}("undefined"!=typeof window?window:this,function(n,t){function i(n,e){t.extend(this,i)}return i.prototype={},t.Calendar=i});