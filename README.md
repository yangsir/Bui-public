# Bui-public

#### 介绍
#### 1、全功能UI库，包括简洁实用的富文本编辑器，其中bui-editor是全网唯一一个支持浮动文本/图片的富文本。
#### 2、采用JavaScript prototype面向对象编程风格，更接近后端开发人员编程习惯的API风格。
#### 3、适用于前后端分离开发模式，采用完整的语言配置，可轻松实现国际化要求。
#### 4、高效的前端curd抽象封装，配合mvvm双向联动，彻底解放常规的前端表单开发。
#### 5、超低学习成本，采用w3c范畴内的标签，属性构建，不需要学习额外的规则，语法。

#### 软件架构
JavaScript prototype面向对象编程风格。
依赖jquery
依赖fontawesome开源图标库


 **官方网站 ： [www.vvui.net](http://vvui.net)** 


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)